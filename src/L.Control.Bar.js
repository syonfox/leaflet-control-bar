/**
 *
 * @param initialization can be HTMLElement, cssSelector or html sting, falsy is a new empty element
 * and somtine it fills in an error on edge cases.
 *
 * note if string is passes if there is only one element it is returned.
 * if many or non the parent wraper is returned
 *
 * @return {HTMLElement}
 */
function initializeElement(initialization) {
    const temp = document.createElement('div');
    temp.className = 'bar-content-parent';

    if (initialization instanceof HTMLElement) {
        // If the initialization is already a DOM element, return it
        return initialization;
    } else if (typeof initialization === 'string') {
        try {
            // Try to select the element using the initialization string as a CSS selector
            var element = document.querySelector(initialization);


            if (element) {
                // If the selector matches an element, return it
                return element;
            } else {
                element = document.getElementById(initialization);
                if (element) return element; //preserve id alone working
                // If the selector doesn't match any element, set the innerHTML of the temp div to the initialization string
                console.warn(`No element found for selector '${initialization}'`);
                temp.innerHTML = `No element found for selector : '${initialization}'`;
            }
        } catch (e) {
            // If the initialization is not a valid CSS selector, set the innerHTML of the temp div to the error message
            console.log(`Invalid CSS selector assuming html string: '${initialization}'`);

            temp.innerHTML = initialization.trim();
            if (temp.childElementCount === 1) {
                // If there is only one child element, return it
                return temp.firstElementChild;
            } // else there is 0 and we are returning a wrapped text or there are many and they need a container

        }
    } else if (initialization) {
        // If the initialization is not null, undefined, or a valid type, set the innerHTML of the temp div to the error message
        console.warn('Invalid initialization type');
        if (initialization) {
            temp.innerHTML = 'Invalid initialization type';
        }
    }

    // Return the temp div with the error message as its innerHTML
    return temp;
}


/**
 * Created by zava on 7.6.2015. https://github.com/filipzava/leaflet-control-bar
 * updated by syonfox on 3.9.2020 https://gitlab.com/syonfox/leaflet-control-bar
 */
L.Control.Bar = L.Control.extend({

    includes: L.Mixin.Events,

    options: {
        position: 'top',
        visible: 'true',
        resizable: 'false',
        height: 250,
        handleHeight: 20,
        handleHTML: ''
    },


    initialize: function (placeholder, options) {
        L.setOptions(this, options);

        // Find content container not this is a breaking change. since before we jus accepted an id
        // var content = initializeElement(placeholder)//broken dont use yet idk what leaflet does but its special
        var content = this._contentContainer = L.DomUtil.get(placeholder);


        this.EVENT = {
            DOWN: "mousedown",
            UP: "mouseup",
            MOVE: "mousemove"
        }

        if (options.usePointer) {
            this.EVENT = {
                DOWN: "pointerdown",
                UP: "pointerup",
                MOVE: "pointermove"
            }
        }

        this.offsets = {
            leafletControl: {
                top: 5,
                bottom: 5,
                left: 5,
                right: 5,
            },

            positionShow: {
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
            },
            positionHide: {
                top: 5,
                bottom: 5,
                left: 5,
                right: 5,
            },
            height: {
                top: 5,
                bottom: 5,
                left: 5,
                right: 5,
            }
        }

        // this._height = this.options.height;
        // Remove the content container from its original parent
        content.parentNode.removeChild(content);

        // var visibileClass = (this.options.visible) ? 'visible' : '';
        // this.visible = this.options.visible;

        // Create sidebar container
        var container = this._container =
            L.DomUtil.create('div', 'leaflet-control-bar-' + this.options.position + ' leaflet-control-bar ');


        // this.resize(this.options.height)

        this._setHeight(this.options.height);

        if (this.options.visible) {
            this.show();
        } else {
            this.hide();
        }

        // Style and attach content container
        L.DomUtil.addClass(content, 'leaflet-control');

        if (this.options.resizable) {

            switch (this.options.position) {
                case "top": {
                    let handle = this._resizeHandle = L.DomUtil.create('div', 'leaflet-control-bar-handle');
                    handle.innerHTML = this.options.handleHTML;
                    handle.style.height = this.options.handleHeight + 'px';
                    handle.style.minHeight = this.options.handleHeight + 'px';
                    handle.style.width = '100%';

                    container.appendChild(content);
                    container.appendChild(handle);
                    break;
                }
                case "bottom": {
                    let handle = this._resizeHandle = L.DomUtil.create('div', 'leaflet-control-bar-handle');
                    handle.innerHTML = this.options.handleHTML;
                    handle.style.height = this.options.handleHeight + 'px';
                    handle.style.minHeight = this.options.handleHeight + 'px';
                    handle.style.width = '100%';

                    container.appendChild(handle);
                    container.appendChild(content);

                    break;
                }
                case "left": {
                    let handle = this._resizeHandle = L.DomUtil.create('div', 'leaflet-control-bar-handle');
                    handle.innerHTML = this.options.handleHTML;
                    handle.style.height = '100%';
                    handle.style.width = this.options.handleHeight + 'px';
                    handle.style.minWidth = this.options.handleHeight + 'px';

                    container.appendChild(content);
                    container.appendChild(handle);
                    break;
                }
                case "right": {
                    let handle = this._resizeHandle = L.DomUtil.create('div', 'leaflet-control-bar-handle');
                    handle.innerHTML = this.options.handleHTML;
                    handle.style.height = '100%';
                    handle.style.width = this.options.handleHeight + 'px';
                    handle.style.minWidth = this.options.handleHeight + 'px';

                    container.appendChild(handle);
                    container.appendChild(content);
                    break;
                }

            }

        } else {
            container.appendChild(content);
        }


    },

    addTo: function (map) {
        let container = this._container;
        let content = this._contentContainer;
        L.DomEvent
            .on(container, 'transitionend',
                this._handleTransitionEvent, this)
            .on(container, 'webkitTransitionEnd',
                this._handleTransitionEvent, this);

        //

        if (this.options.resizable) {
            let resizeHandle = this._resizeHandle;
            // resizeHandle.innerHTML = '<=====-^-=====>'
            // document.body.appendChild(resizeHandle)
            this._moved = false;
            let self = this
            //if they touch the handle start tacking the drag till they release
            this._downListener = (e) => {
                console.log(this.EVENT.DOWN)
                self._moved = false
                self._mousedown = true;

                if (self.options.usePointer) {
                    self._map._container.addEventListener(self.EVENT.MOVE, self._moveListener);
                    self._map._container.addEventListener(self.EVENT.UP, self._upListener, {once: true})

                } else {
                    //https://github.com/Leaflet/Leaflet/issues/7200#issuecomment-1498595970
                    self._map.on(self.EVENT.MOVE, self._moveListener)
                    self._map.once(self.EVENT.UP, self._upListener)

                }

                self.fire('resizeStart')
            }
            resizeHandle.addEventListener(this.EVENT.DOWN, this._downListener)
            this._moveListener = (e) => {
                // console.log('move')
                // console.log(e);

                if (!e.containerPoint) e.containerPoint = self._map.mouseEventToContainerPoint(e);

                if (self._mousedown) {

                    let height;
                    switch (this.options.position) {
                        case "top": {
                            height = e.containerPoint.y;
                            break;
                        }
                        case "bottom": {
                            height = self._map._container.clientHeight - e.containerPoint.y;
                            break;
                        }
                        case "left": {
                            height = e.containerPoint.x;
                            break;
                        }
                        case "right": {
                            height = self._map._container.clientWidth - e.containerPoint.x;
                            break;
                        }

                    }
                    self.resize(height);
                }
                self._moved = true
            }

            this._upListener = (e) => {
                self._mousedown = false;
                console.log("up")
                if (self._moved) {
                    console.log('moved')
                    console.log(e);
                } else {
                    console.log('not moved')
                }

                //this forces content to update
                // if(self.options.windowResize) {
                //     window.dispatchEvent(new Event('resize'));
                // }
                if (self.options.usePointer) {
                    self._map._container.removeEventListener(self.EVENT.MOVE, self._moveListener)
                } else {
                    //https://github.com/Leaflet/Leaflet/issues/7200#issuecomment-1498595970
                    self._map.off(self.EVENT.MOVE, self._moveListener)
                }
                self.fire('resizeEnd');
            }
        }

        var controlContainer = map._controlContainer;
        controlContainer.insertBefore(container, controlContainer.firstChild);

        this._map = map;

        // Make sure we don't drag the map when we interact with the content
        var stop = L.DomEvent.stopPropagation;
        L.DomEvent
            .on(content, 'contextmenu', stop)
            .on(content, 'click', stop)
            .on(content, this.EVENT.DOWN, stop)
            .on(content, 'touchstart', stop)
            .on(content, 'dblclick', stop)
            .on(content, 'mousewheel', stop)
            .on(content, 'MozMousePixelScroll', stop);

        return this;
    },

    removeFrom: function (map) {
        //if the control is visible, hide it before removing it.
        this.hide();

        let content = this._contentContainer;
        let container = this._container;

        // Remove sidebar container from controls container
        var controlContainer = map._controlContainer;
        controlContainer.removeChild(this._container);

        //disassociate the map object
        this._map = null;

        // Unregister events to prevent memory leak
        var stop = L.DomEvent.stopPropagation;
        L.DomEvent
            .off(content, 'contextmenu', stop)
            .off(content, 'click', stop)
            .off(content, this.EVENT.DOWN, stop)
            .off(content, 'touchstart', stop)
            .off(content, 'dblclick', stop)
            .off(content, 'mousewheel', stop)
            .off(content, 'MozMousePixelScroll', stop);

        L.DomEvent
            .off(container, 'transitionend',
                this._handleTransitionEvent, this)
            .off(container, 'webkitTransitionEnd',
                this._handleTransitionEvent, this);


        // release memory
        if (this.options.resizable) {
            let resizeHandle = this._resizeHandle;
            resizeHandle.removeEventListener(this.EVENT.DOWN, this._downListener)
            resizeHandle.removeEventListener(this.EVENT.MOVE, this._moveListener)
            resizeHandle.removeEventListener(this.EVENT.UP, this._upListener)
        }
        return this;
    },

    isVisible: function () {
        return L.DomUtil.hasClass(this._container, 'visible');
    },

    show: function () {
        if (!this.isVisible()) {
            L.DomUtil.addClass(this._container, 'visible');

            let bottoms = document.getElementsByClassName(`leaflet-${this.options.position}`);
            for (let i = 0; i < bottoms.length; i++) {
                bottoms[i].style.transition = `${this.options.position} 0.5s`;
                bottoms[i].style[this.options.position] = this._height + this.offsets.leafletControl[this.options.position] + 'px';
            }
            this._container.style[this.options.position] = this.offsets.positionShow[this.options.position] + 'px';

            this.fire('show');
        }
    },

    hide: function (e) {
        if (this.isVisible()) {
            L.DomUtil.removeClass(this._container, 'visible');
            this.fire('hide');


            let bottoms = document.getElementsByClassName(`leaflet-${this.options.position}`);
            for (let i = 0; i < bottoms.length; i++) {
                bottoms[i].style.transition = `${this.options.position} 0.5s`;
                bottoms[i].style[this.options.position] = 0 + 'px';
            }
            this._container.style[this.options.position] = -this._height - this.offsets.positionHide[this.options.position] + 'px';

        }
        if (e) {
            L.DomEvent.stopPropagation(e);
        }

    },

    resize: function (height) {
        // let self = this;
        this.fire('resize');
        this._setHeight(height);
        // this._container.style.height = height + 5 + 'px';
        let bottoms = document.getElementsByClassName(`leaflet-${this.options.position}`);
        for (let i = 0; i < bottoms.length; i++) {
            bottoms[i].style.transition = `${this.options.position} 0s`;
            bottoms[i].style[this.options.position] = height + this.offsets.leafletControl[this.options.position] + 'px';
            // bottoms[i].style.transition = 'bottom 0.5s';

        }
        this.fire('resized');
    },

    toggle: function () {
        if (this.isVisible()) {
            this.hide();
        } else {
            this.show();
        }
    },

    getContainer: function () {
        return this._contentContainer;
    },

    setContent: function (content) {
        this.getContainer().innerHTML = content;
        return this;
    },
    _updateLeafletControls: function () {
        //todo break out the controle update into its own function
    },
    _setHeight: function (height) {
        if (this.options.position === 'top' || this.options.position === 'bottom') {
            this._container.style.height = height + this.offsets.height[this.options.position] + 'px';
        } else {
            this._container.style.width = height + this.offsets.height[this.options.position] + 'px';
        }
        this._height = height;
    }
    ,
    _handleTransitionEvent: function (e) {
        if (e.propertyName === 'left' || e.propertyName === 'right' || e.propertyName === 'bottom' || e.propertyName === 'top')
            this.fire(this.isVisible() ? 'shown' : 'hidden');
    }
});

L.control.bar = function (placeholder, options) {
    return new L.Control.Bar(placeholder, options);
};