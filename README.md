leaflet-control-bar
=====================

<a href="https://www.npmjs.com/package/leaflet-control-bar">
<img src="https://img.shields.io/npm/v/leaflet-control-bar.svg" alt="npm version">
</a>

[![Gitlab last commit](https://img.shields.io/gitlab/last-commit/syonfox/leaflet-control-bar)](https://gitlab.com/syonfox/leaflet-control-bar/commits)
[![gitlab](https://img.shields.io/badge/GitLab-100000?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/syonfox/leaflet-control-bar)


Simple navigation bar for excellent javascript mapping library [Leaflet](http://leafletjs.com).
with the added functionality of resizing, allowing the user to change the height of the control bar.
some development sponsored by [vgeo.ca](https://vgeo.ca) we started [endpoint.ca](https://endpoint.ca)

*Compatible with Leaflet 0.6.0 or newer*
*Now supports pointer events for a unified drag experience*

## Example
[Check out demo!](https://syonfox.gitlab.io/leaflet-control-bar/example/)

## Using the plugin

### Install

You can install with [npm](https://www.npmjs.com/package/leaflet-control-bar) 
```
npm i leaflet-control-bar
```
or just download the source files from src and add them to your project

then you can include it with something like this
```html
    <link rel="stylesheet" type="text/css" href="node_modules/leaflet-control-bar/src/L.Control.Bar.css">
    <script type="text/javascript" src="node_modules/leaflet-control-bar/src/L.Control.Bar.js"></script>
```

### Usage
Create an element
```html
 <div id="bar"></div>
```
Create a new L.Control.Bar object and append to the map  
```javascript
var controlBar = L.control.bar('bar',{
    position:'top',
    visible:true
});
map.addControl(controlBar);
```

### Options
* **position**:
    * top  *(default)*
    * bottom
    * left
    * right

* **visibility**: 
    * true *visible on add (default)*
    * false
    
 * **resizable**: 
    * true 
    * false  *not resizable on add (default)*
    
 * **height**: 
    * int *height/width of the bar in picles (250 default)*
    
 * **handleHeight**: 
    * int *height/width resize handle (20 default)*
 * **handleHTML**: 
    * string *the html that goes in the resize handle (''' default)*
 * **usePointer**: 
    * bool * if true will use new pointer events might not work in legacy leaflet ('false' default)*

[//]: # (todo abandon leaflet event system completely and use dom events :&#41; )

### Methods
~~~~javascript
// Show control bar
controlBar.show();

// Hide control bar
controlBar.hide();

// Toggle control bar visibility
controlBar.toggle();

// Resizes the bar to 'height' pixels
controlBar.resize(height);

// Check control bar visibility
var visible = controlBar.isVisible();

// Set content to control bar
controlBar.setContent('<p>This is sample content :)</p>');
~~~~
 
 
### Events

~~~~javascript
controlBar.on('shown', function () {
    console.log('Hello control bar!');
});
~~~~ 

Available events:

- **show**: Show animation is starting, bar will be visible.
- **shown**: Show animation finished, bar is now visible.
- **hide**: Hide animation is starting, bar will be hidden.
- **hidden**: Hide animation finished, bar is now hidden.
- **resize**: Resize start
- **resized**: Resize finished
- **resizeStart**: Fired when initial mouse down happens, and we start tracking the movements.
- **resizeEnd**: Fired when Final mouse up happens, and we stop tracking the movements. (this is where you would want to redraw)
     ```js
  //example
    bar.on('resizeEnd', (e)=> {  
      window.dispatchEvent(new Event('resize')); 
    });
    ```

### License

**leaflet-control-bar** is free software, and may be redistributed under the MIT-LICENSE.

A fork of the [leaflet-control-bar](https://github.com/filipzava/leaflet-control-bar) plugin.
Inspired by [leaflet-sidebar](https://github.com/Turbo87/leaflet-sidebar) plugin.